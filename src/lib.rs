#[macro_use]
extern crate nom;
extern crate crc;

use nom::{le_u16, le_u32};

named!(detect_sof, tag!(&[0x53, 0x4F, 0x46, 0x00]));

named!(read_chunk<(u16, u32, &[u8])>,
    do_parse! (
        id: le_u16 >> 
        len: le_u32 >>
        data: take!(len) >>
        (id, len, data)
    )
);

const ID_TOOL_NAME : u16 = 1; // string with the tool name and version
const ID_PART_NAME : u16 = 2; // string with the part number
const ID_UNTITLED : u16 = 3; // string "untitled"
const ID_UNKNOWN0 : u16 = 18; // unkown chunk with 96 bytes full with FF and 00
const ID_UNKNOWN1 : u16 = 19; // unkown chunk with 16 bytes mostly full with 00
const ID_UNKNOWN2 : u16 = 36; // unkown chunk with 385 bytes 
const ID_UNKNOWN3 : u16 = 17; // unkown large chunk (660787) - probably the bitstream
const ID_UNKNOWN4 : u16 = 24; // unkown large chunk (573452) - probably the block RAM (device has 549 KBit) 
const ID_UNKNOWN5 : u16 = 21; // unkown large chunk (1729) - probably memory
const ID_UNKNOWN6 : u16 = 52; // unkown small chunk with 0F 00 00 00
const ID_UNKNOWN7 : u16 = 53; // unkown small chunk with 00 00 17 00
const ID_UNKNOWN8 : u16 = 56; // unkown small chunk with 1C 14 00 00 00 00 00 00
const ID_CRC16 : u16 = 8; // CRC-16 (X.25) over the complete file except the two CRC-bytes

#[cfg(test)]
mod tests {
    use crc::crc16;
    use crc::*;

    #[test]
    fn tst_crc() {
        let data = include_bytes!("../crc-test.sof");

        // the last chunk contains the CRC-16
        let len = data.len() - 2;
        let content = &data[0..len];
        let file_crc = ((data[len + 1] as u16) << 8) | (data[len] as u16);

        let crc = crc16::update(0x0000u16, &crc16::X25_TABLE, &content);
        println!("calculated CRC: {:X}", crc);
        println!("file CRC: {:X}", file_crc);

        assert_eq!(crc, file_crc);
    }

}
